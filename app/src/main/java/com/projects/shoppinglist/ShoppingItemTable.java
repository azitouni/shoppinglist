package com.projects.shoppinglist;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Aziz on 10/13/2015.
 */
public class ShoppingItemTable extends DBHelper {
    public static final String TABLE_SHOPPING_ITEM = "item";
    public static final String COLUMN_ITEM_NAME = "name";
    public static final String COLUMN_ITEM_PRICE = "price";

    public static final String PREFIX = "sitem";
    public static final String ITEM_ID_WITH_PREFIX = PREFIX + "." + COLUMN_ID;
    public static final String ITEM_NAME_WITH_PREFIX = PREFIX + "." + COLUMN_ITEM_NAME;
    public static final String ITEM_PRICE_WITH_PREFIX = PREFIX + "." + COLUMN_ITEM_PRICE;

    @Override
    public void createTable(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_SHOPPING_ITEM + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_ITEM_NAME + " TEXT NOT NULL, " +
                COLUMN_ITEM_PRICE + " REAL" +
                ")";
        db.execSQL(query);
    }

    @Override
    public void upgradeTable(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHOPPING_ITEM + ";");
    }
}
