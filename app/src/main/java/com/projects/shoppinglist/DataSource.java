package com.projects.shoppinglist;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Aziz on 11/16/2015.
 */
public class DataSource {
    protected SQLiteDatabase database;
    private DBHandler dbHandler;
    private Context mContext;

    public DataSource(Context context) {
        this.mContext = context;
        open();
    }

    public void open() {
        dbHandler = DBHandler.getInstance(mContext);
        database = dbHandler.getWritableDatabase();
    }

    public void close() {
        dbHandler.close();
    }

}
