package com.projects.shoppinglist;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Aziz on 10/13/2015.
 */
public class ShoppingHistoryTable extends DBHelper {
    public static final String TABLE_SHOPPING_HISOTRY = "shoppinghistory";

    public static final String COLUMN_ITEM_ID = "itemid";
    public static final String COLUMN_DATE = "shoppingdate";
    public static final String COLUMN_ITEM_QUANTITY = "itemquantity";
    public static final String COLUMN_LABEL = "listlabel";

    public static final String PREFIX = "shist";
    public static final String ENTRY_ID_WITH_PREFIX = PREFIX + "." + COLUMN_ID;
    public static final String ITEM_ID_WITH_PREFIX = PREFIX + "." + COLUMN_ITEM_ID;
    public static final String COLUMN_DATE_WITH_PREFIX = PREFIX + "." + COLUMN_DATE;
    public static final String COLUMN_ITEM_QUANTITY_WITH_PREFIX = PREFIX + "." + COLUMN_ITEM_QUANTITY;
    public static final String COLUMN_LABEL_WITH_PREFIX = PREFIX + "." + COLUMN_LABEL;

    @Override
    public void createTable(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_SHOPPING_HISOTRY + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_LABEL + " TEXT NOT NULL, " +
                ShoppingItemTable.COLUMN_ITEM_NAME + " TEXT, " +
                COLUMN_DATE + " DATETIME, " +
                COLUMN_ITEM_ID + " INTEGER, " +
                COLUMN_ITEM_QUANTITY + " INTEGER, " +
                " FOREIGN KEY (" + COLUMN_ITEM_ID + ") REFERENCES " +
                ShoppingItemTable.TABLE_SHOPPING_ITEM + " (" + COLUMN_ID + ")" +
                ");";
        db.execSQL(query);
    }

    @Override
    public void upgradeTable(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHOPPING_HISOTRY + ";");
    }
}
