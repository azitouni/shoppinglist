package com.projects.shoppinglist;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class ShoppingListDetailActivity extends AppCompatActivity {

    ShoppingHistoryDataSource datasource;
    DBHandler dbHandler;
    List<ShoppingHistoryEntry> entries;
    ListView shoppingItemsList;
    double total = -1;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, UserPreferencesActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == android.R.id.home) {
            goToMainActivity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_list_detail);

        datasource = new ShoppingHistoryDataSource(this);

        Bundle bundle = getIntent().getExtras();
        String shopListLabel = bundle.getString("label");
        getSupportActionBar().setTitle(shopListLabel);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        entries = datasource.getItemsByLabel(shopListLabel);
        displayTotalEstimatedCost();

        ShoppingListAdapter adapter = new ShoppingListAdapter(this, entries);
        shoppingItemsList = (ListView) findViewById(R.id.shoppingItemsList);
        shoppingItemsList.setAdapter(adapter);
    }

    private double calculateTotalEstimatedCost() {
        total = 0.0;
        for (ShoppingHistoryEntry entry : entries) {
            total += entry.getItem().getPrice() * entry.getQuantity();
        }
        return total;
    }

    private void displayTotalEstimatedCost() {
        TextView totalCostTxt = (TextView) findViewById(R.id.txt_estimated_cost);
        totalCostTxt.setText(Util.roundDouble3Digits(calculateTotalEstimatedCost()));
    }

    public void onClick_includeOrExcludeTaxes(View view) {
        boolean includeTaxes = ((CheckBox) findViewById(R.id.chk_include_taxes)).isChecked();
        TextView totalCostTxt = (TextView) findViewById(R.id.txt_estimated_cost);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        double taxes = Double.parseDouble(preferences.getString(getString(R.string
                        .pref_key_tax_percentage), "0.0"));
        if (total == -1) {
            calculateTotalEstimatedCost();
        }
        if (includeTaxes) {
            double totalCostWithTaxes = total + total * (taxes / 100.0);
            totalCostTxt.setText(String.valueOf(totalCostWithTaxes));
        }
        else {
            totalCostTxt.setText(String.valueOf(total));
        }

    }
}
