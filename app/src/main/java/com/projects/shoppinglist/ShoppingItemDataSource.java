package com.projects.shoppinglist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.lang.ref.PhantomReference;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Aziz on 10/13/2015.
 */
public class ShoppingItemDataSource extends DataSource {

    private String[] allColumns = {ShoppingItemTable.COLUMN_ID,
            ShoppingItemTable.COLUMN_ITEM_NAME, ShoppingItemTable.COLUMN_ITEM_PRICE};

    private static final int COLUMN_ID_INDEX = 0;
    private static final int COLUMN_NAME_INDEX = 1;
    private static final int COLUMN_PRICE_INDEX = 2;

    public ShoppingItemDataSource(Context context) {
        super(context);
    }

    public ShoppingItem addItem(String name, double price) {
        ContentValues values = new ContentValues();
        values.put(ShoppingItemTable.COLUMN_ITEM_NAME, name);
        values.put(ShoppingItemTable.COLUMN_ITEM_PRICE, price);
        long id = database.insert(ShoppingItemTable.TABLE_SHOPPING_ITEM, null, values);
        Cursor cursor = database.query(
                ShoppingItemTable.TABLE_SHOPPING_ITEM,
                allColumns,
                ShoppingItemTable.COLUMN_ID + " = " + id,
                null,
                null,
                null,
                null);
        cursor.moveToFirst();
        ShoppingItem sItem = cursorToShoppingItem(cursor);
        cursor.close();
        return sItem;
    }

    private boolean isItemExists(String name) {
        String query = "SELECT " + ShoppingItemTable.COLUMN_ID + " FROM " + ShoppingItemTable.TABLE_SHOPPING_ITEM +
                " WHERE " + ShoppingItemTable.COLUMN_ITEM_NAME + " = ?;";

        Cursor cursor = database.rawQuery(query, new String[]{name});
        boolean exists = (cursor.getCount() > 0);
        cursor.close();
        return exists;
    }

    public void deleteItem(ShoppingItem item) {
        long id = item.get_id();
        database.delete(ShoppingItemTable.TABLE_SHOPPING_ITEM, ShoppingItemTable.COLUMN_ID + " = " + id, null);
//        database.execSQL("DELETE FROM " + ShoppingItemTable.TABLE_SHOPPING_ITEM + " WHERE "
//                + DBHandler.COLUMN_ITEM_NAME + " = \"" + itemName + "\";");
    }

    public List<ShoppingItem> getAllItems() {
        List<ShoppingItem> items = new ArrayList<ShoppingItem>();
        Cursor cursor = database.query(ShoppingItemTable.TABLE_SHOPPING_ITEM, allColumns, null, null, null,
                null, null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            ShoppingItem item = cursorToShoppingItem(cursor);
            items.add(item);
            cursor.moveToNext();
        }

        cursor.close();
        return items;
    }

    private ShoppingItem cursorToShoppingItem(Cursor cursor) {
        ShoppingItem item = new ShoppingItem(cursor.getLong(COLUMN_ID_INDEX),
                cursor.getString(COLUMN_NAME_INDEX), cursor.getDouble(COLUMN_PRICE_INDEX));
        return item;
    }

    public static ShoppingItem getItemById(SQLiteDatabase database, long id) {
        Cursor cursor = database.query(ShoppingItemTable.TABLE_SHOPPING_ITEM,
                null, DBHelper.COLUMN_ID + " = " + id, null, null, null, null);
        cursor.moveToFirst();
        long item_id = cursor.getLong(COLUMN_ID_INDEX);
        String item = cursor.getString(COLUMN_NAME_INDEX);
        double price = cursor.getDouble(COLUMN_PRICE_INDEX);
        cursor.close();
        return new ShoppingItem(item_id, item, price);
    }
}
