package com.projects.shoppinglist;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Aziz on 10/13/2015.
 */
public abstract class DBHelper {
    public static final String COLUMN_ID = "_id";

    public abstract void createTable(SQLiteDatabase db);
    public abstract void upgradeTable(SQLiteDatabase db);

}
