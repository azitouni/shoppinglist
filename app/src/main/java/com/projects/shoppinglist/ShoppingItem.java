package com.projects.shoppinglist;

/**
 * Created by Aziz on 8/30/2015.
 */
public class ShoppingItem {

    private String _name;
    private long _id;
    private double price;

    public ShoppingItem() {
        _name = "";
        _id = -1;
        price = 0.0;
    }
    public ShoppingItem(long id, String name) {
        _id = id;
        _name = name;
    }

    public ShoppingItem(long id, String name, double price) {
        _id = id;
        _name = name;
        this.price = price;
    }

    public void set_id(long id) {
        _id = id;
    }

    public long get_id() {
        return _id;
    }

    public String getName() {
        return _name;
    }

    public void setName(String _name) {
        this._name = _name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return _name;
    }

}
