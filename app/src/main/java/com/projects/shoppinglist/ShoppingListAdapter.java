package com.projects.shoppinglist;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Aziz on 10/15/2015.
 */
public class ShoppingListAdapter extends ArrayAdapter<ShoppingHistoryEntry> {

    private Context context;
    List<ShoppingHistoryEntry> items;

    public ShoppingListAdapter(Context context, List<ShoppingHistoryEntry> items) {
        super(context, R.layout.shopping_list_entry);
        this.context = context;
        this.items = items;
    }

    private class ViewHolder {
        TextView itemName;
        TextView itemQuantity;
        CheckBox itemInCart;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public ShoppingHistoryEntry getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(
                    Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.shopping_list_entry, null);
            holder = new ViewHolder();

            holder.itemName = (TextView) convertView.findViewById(R.id.txt_item_name);
            holder.itemQuantity = (TextView) convertView.findViewById(R.id.txt_item_quantity);
            holder.itemInCart = (CheckBox) convertView.findViewById(R.id.chk_items_in_cart);
            final ViewHolder finalHolder = holder;
            holder.itemInCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CheckBox chBox = (CheckBox) v;
                    if (chBox.isChecked()) {
                        finalHolder.itemName.setTextColor(Color.GRAY);
                        finalHolder.itemQuantity.setTextColor(Color.GRAY);
                    }
                    else {
                        finalHolder.itemName.setTextColor(Color.BLACK);
                        finalHolder.itemQuantity.setTextColor(Color.BLACK);
                    }
                }
            });
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
        ShoppingHistoryEntry entry = getItem(position);
        holder.itemName.setText(entry.getItem().getName());
        holder.itemQuantity.setText(String.valueOf(entry.getQuantity()));

        return convertView;
    }
}
