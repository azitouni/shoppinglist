package com.projects.shoppinglist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button addNewListBtn = (Button) findViewById(R.id.btn_addNewList);
        Button browseListsBtn = (Button) findViewById(R.id.btn_BrowseLists);
        addNewListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddNewListActivity.class);
                startActivity(intent);
                finish();
            }
        });
        browseListsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BrowseListsActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

}
