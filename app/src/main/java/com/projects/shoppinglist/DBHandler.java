package com.projects.shoppinglist;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aziz on 8/30/2015.
 */
public class DBHandler extends SQLiteOpenHelper {
    private static DBHandler dbHandlerInstance;

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "shoppinglist.db";
    private static List<DBHelper>tables = new ArrayList<DBHelper>();
    static {
        tables.add(new ShoppingItemTable());
        tables.add(new ShoppingHistoryTable());
    }

    private SQLiteDatabase database;
    private DBHandler(Context context, String name,
                     SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    private DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
//        context.deleteDatabase(DATABASE_NAME);
    }

    public static synchronized DBHandler getInstance(Context context) {
        if (dbHandlerInstance == null) {
            dbHandlerInstance = new DBHandler(context.getApplicationContext());
        }
        return dbHandlerInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (DBHelper table : tables) {
            table.createTable(db);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (DBHelper table : tables) {
            table.upgradeTable(db);
        }
        onCreate(db);
    }


}
