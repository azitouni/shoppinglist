package com.projects.shoppinglist;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddNewListActivity extends AppCompatActivity {
    private EditText editTxtItemName;
    private Spinner spnQuantity;
    private EditText editTxtPrice;
    private TableLayout itemsContainer;

    private TextView estimatedCostLabel;
    private TextView estimatedCost;

    private ShoppingItemDataSource shoppingItemDS;
    private ShoppingHistoryDataSource shoppingHistDS;

    private double totalCostNoTax = 0.0;
    private double priceTaxes = 0.0;
    private static final int NUMBERS_SPINNER_MAX = 1000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_list);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        editTxtItemName = (EditText) findViewById(R.id.editTxt_itemName);
        spnQuantity = (Spinner) findViewById(R.id.spn_itemQuantity);
        initQuantitySpinner();
        editTxtPrice = (EditText) findViewById(R.id.editTxt_itemPrice);
        itemsContainer = (TableLayout) findViewById(R.id.itemsContainer);
        estimatedCost = (TextView) findViewById(R.id.txt_estimated_cost);
        estimatedCost.setText(String.valueOf(getEstimatedTotalCost()));
        estimatedCostLabel = (TextView) findViewById(R.id.txt_title_estimated_cost);

        shoppingItemDS = new ShoppingItemDataSource(this);
        shoppingHistDS = new ShoppingHistoryDataSource(this);
    }

    private double getEstimatedTotalCost() {
        boolean withTaxes = ((CheckBox) findViewById(R.id.chk_include_taxes)).isChecked();
        if (withTaxes) {
            double priceWithTaxes = totalCostNoTax + totalCostNoTax * (priceTaxes / 100.0);
            return priceWithTaxes;
        }
        return totalCostNoTax;
    }

    private void initQuantitySpinner() {
        ArrayAdapter<Integer>numbersArrAdapter = new ArrayAdapter<Integer>(this,
                android.R.layout.simple_list_item_1, numbers(NUMBERS_SPINNER_MAX));
        spnQuantity.setAdapter(numbersArrAdapter);
    }

    private Integer[] numbers(int max) {
        Integer[]n = new Integer[max];
        for (int i = 0; i < max; i++) {
            n[i] = i + 1;
        }
        return n;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_new_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, UserPreferencesActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == android.R.id.home) {
            goToMainActivity();
            return true;
        }
        if (id == R.id.action_save) {
            int children = itemsContainer.getChildCount();
            List<ShoppingItem> items = new ArrayList<ShoppingItem>();
            Map<ShoppingItem, Integer> quantities = new HashMap<ShoppingItem, Integer>();
            String date = Util.getDateTime();
            for (int i = 0; i < children; i++) {
                TableRow row = (TableRow) itemsContainer.getChildAt(i);

                TextView itemTxt = (TextView) row.getChildAt(0);
                TextView quantityTxt = (TextView) row.getChildAt(1);
                TextView priceTxt = (TextView) row.getChildAt(2);

                int quantity = Integer.parseInt(quantityTxt.getText().toString());
                String name = itemTxt.getText().toString();
                double price = Double.parseDouble(priceTxt.getText().toString());

                ShoppingItem sItem = shoppingItemDS.addItem(name, price);
                items.add(sItem);
                quantities.put(sItem, quantity);
            }
            displayListNamePrompt(items, quantities, date);
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickSave(View view) throws InterruptedException {
        int children = itemsContainer.getChildCount();
        List<ShoppingItem> items = new ArrayList<ShoppingItem>();
        Map<ShoppingItem, Integer> quantities = new HashMap<ShoppingItem, Integer>();
        String date = Util.getDateTime();
        for (int i = 0; i < children; i++) {
            TableRow row = (TableRow) itemsContainer.getChildAt(i);

            TextView itemTxt = (TextView) row.getChildAt(0);
            TextView quantityTxt = (TextView) row.getChildAt(1);
            TextView priceTxt = (TextView) row.getChildAt(2);

            int quantity = Integer.parseInt(quantityTxt.getText().toString());
            String name = itemTxt.getText().toString();
            double price = Double.parseDouble(priceTxt.getText().toString());

            ShoppingItem item = shoppingItemDS.addItem(name, price);
            items.add(item);
            quantities.put(item, quantity);
        }
        displayListNamePrompt(items, quantities, date);
    }

    public void onClickAddItem(View view) {
        if (validTxtFields(editTxtItemName)) {
            int quantity = (int) spnQuantity.getSelectedItem();
            String item = editTxtItemName.getText().toString();
            double price = 0.0;
            if (!isEmptyEditText(editTxtPrice)) {
                price = Double.parseDouble(editTxtPrice.getText().toString());
                addNewItemCost(price, quantity);
            }
            if (isEmptyItemContainer()) {
                showItemsContainer();
            }
            itemsContainer.addView(newItemRow(item, quantity, price));
            Toast.makeText(this, R.string.info_item_added, Toast.LENGTH_SHORT).show();
//            editTxtItemName.setText(null);
//            editTxtQuantity.setText(null);
//            editTxtPrice.setText(null);
        }
    }

    private void showItemsContainer() {
        LinearLayout tableHeaders = (LinearLayout) findViewById(R.id.table_headers);
        tableHeaders.setVisibility(View.VISIBLE);
    }

    private boolean isEmptyItemContainer() {
        return itemsContainer.getChildCount() == 0;
    }

    private TextView createItemTextView(String name) {
        TableRow.LayoutParams params = new TableRow.LayoutParams(0,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.rightMargin = 60;
        params.weight = 1;
        TextView txtItem = new TextView(this);
        txtItem.setText(name);
        txtItem.setLayoutParams(params);

        return txtItem;
    }

    private TextView createQuantityTextView(int quantity) {
        TableRow.LayoutParams params = new TableRow.LayoutParams(0,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.rightMargin = 60;
        params.weight = 1;
        TextView txtItemQuantity = new TextView(this);
        txtItemQuantity.setText(String.valueOf(quantity));
        txtItemQuantity.setLayoutParams(params);

        return txtItemQuantity;
    }

    private TextView createPriceTextView(double price) {
        TableRow.LayoutParams params = new TableRow.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.weight = 1;
        TextView txtItemPrice = new TextView(this);
        txtItemPrice.setText(String.valueOf(price));
        txtItemPrice.setLayoutParams(params);

        return txtItemPrice;
    }

    private TableRow newItemRow(String itemName, int itemQuantity, double price) {
        final TableRow row = new TableRow(this);
        TableLayout.LayoutParams row_params = new TableLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        row.setLayoutParams(row_params);
        row.addView(createItemTextView(itemName));
        row.addView(createQuantityTextView(itemQuantity));
        row.addView(createPriceTextView(price));

        Button btnDelete = new Button(this);
        TableRow.LayoutParams btn_params = new TableRow.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        btnDelete.setLayoutParams(btn_params);
        btnDelete.setText(R.string.label_delete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView itemCostTxt = (TextView) row.getChildAt(2);
                TextView itemQuantityTxt = (TextView) row.getChildAt(1);
                double itemCost = Double.parseDouble(itemCostTxt.getText().toString());
                int quantity = Integer.parseInt(itemQuantityTxt.getText().toString());
                addNewItemCost(-itemCost, quantity);
                itemsContainer.removeView(row);
                // Hide the item containers table headers if only 1 row remains in the table
                if (isEmptyItemContainer()) {
                    hideTableHeaders();
                }
            }
        });
        row.addView(btnDelete);
        return row;
    }

    private void hideTableHeaders() {
        LinearLayout tableHeaders = (LinearLayout) findViewById(R.id.table_headers);
        tableHeaders.setVisibility(View.INVISIBLE);
    }

    private boolean validTxtFields(EditText... fields) {
        for (EditText txt : fields) {
            if (isEmptyEditText(txt)) {
                Toast.makeText(this, R.string.info_invalid_input_empty,
                        Toast.LENGTH_LONG).show();
                return false;
            }
        }
        return true;
    }

    private boolean isEmptyEditText(EditText text) {
        return text.getText().toString().trim().isEmpty();
    }

    private void displayListNamePrompt(final List<ShoppingItem> items,
                                       Map<ShoppingItem, Integer> quantities, final String date) {
        final AlertDialog dlgNamePrompt = shoppingListNameDialog(items, quantities, date);
        dlgNamePrompt.show();
    }

    private AlertDialog shoppingListNameDialog(
            final List<ShoppingItem> items, final Map<ShoppingItem, Integer> quantities,
            final String date) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.title_shopping_list_name);
        alertDialogBuilder.setMessage(R.string.msg_enter_shopping_list_name);

        final EditText input = new EditText(this);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT
        );
        input.setLayoutParams(params);
        input.setLines(1);
        input.setSingleLine(true);
        alertDialogBuilder.setView(input);
        alertDialogBuilder.setPositiveButton(R.string.label_save, null);
        alertDialogBuilder.setNegativeButton(R.string.label_cancel, null);
        final AlertDialog dlgNamePrompt = alertDialogBuilder.create();
        dlgNamePrompt.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button saveBtn = dlgNamePrompt.getButton(AlertDialog.BUTTON_POSITIVE);
                Button cancelBtn = dlgNamePrompt.getButton(AlertDialog.BUTTON_NEGATIVE);
                saveBtn.setOnClickListener(onClickSave_shoppingListNameDialog(items, quantities,
                        date, input, dialog));
                cancelBtn.setOnClickListener(onClickCancel_shoppingListNameDialog(dialog));
            }
        });
        return dlgNamePrompt;
    }

    private View.OnClickListener onClickCancel_shoppingListNameDialog(final DialogInterface dialog) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                dialog.dismiss();
            }
        };
    }

    private View.OnClickListener onClickSave_shoppingListNameDialog(
            final List<ShoppingItem> items, final Map<ShoppingItem, Integer> quantities,
            final String date, final EditText input, final DialogInterface dialog) {
        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isEmptyEditText(input)) {
                    String name = input.getText().toString();
                    if (!shoppingHistDS.isShoppingLabelExists(input.getText().toString())) {
                        saveShoppingListToDb(name, items, quantities, date);
                        Toast.makeText(AddNewListActivity.this,
                                R.string.info_shopping_list_saved, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        goToMainActivity();
                    }
                    else {
                        String message = String.format(
                                getResources().getString(R.string.error_shopping_list_name_exists),
                                name);
                        Toast.makeText(AddNewListActivity.this,
                                message,
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(AddNewListActivity.this,
                            R.string.error_shopping_list_name_empty,
                            Toast.LENGTH_LONG).show();
                }
            }
        };
        return clickListener;
    }

    private void saveShoppingListToDb(String name, List<ShoppingItem> items,
                                      Map<ShoppingItem, Integer> quantities, String date) {
        for (ShoppingItem item : items) {
            ShoppingHistoryEntry newEntry = shoppingHistDS.addShoppingListEntry(date, name, item,
                    quantities.get(item));
        }
    }

    private void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        priceTaxes = Double.parseDouble(preferences.getString(getString(R.string.pref_key_tax_percentage),
                "0.0"));
        updateTotalCostTxt();
        super.onResume();
    }

    private void addNewItemCost(double itemCost, int itemQuantity) {
        totalCostNoTax += (itemCost * itemQuantity);
        updateTotalCostTxt();
    }

    private void updateTotalCostTxt() {
        boolean taxesIncluded = ((CheckBox) findViewById(R.id.chk_include_taxes)).isChecked();
        if (taxesIncluded) {
            double priceWithTaxes = totalCostNoTax + totalCostNoTax * (priceTaxes / 100.0);
            estimatedCost.setText(Util.roundDouble3Digits(priceWithTaxes));
        }
        else {
            estimatedCost.setText(Util.roundDouble3Digits(totalCostNoTax));
        }
    }

    private void updateTotalCostTxt(boolean withTaxes) {
        if (withTaxes) {
            double priceWithTaxes = totalCostNoTax + totalCostNoTax * (priceTaxes / 100.0);
            estimatedCost.setText(Util.roundDouble3Digits(priceWithTaxes));
        }
        else {
            estimatedCost.setText(Util.roundDouble3Digits(totalCostNoTax));
        }
    }

    public void onClick_showOrHideEstimatedCost(View view) {
        CheckBox chBox = (CheckBox) view;
        if (chBox.isChecked()) {
            estimatedCostLabel.setVisibility(View.VISIBLE);
            estimatedCost.setVisibility(View.VISIBLE);
        } else {
            estimatedCostLabel.setVisibility(View.INVISIBLE);
            estimatedCost.setVisibility(View.INVISIBLE);
        }
    }

    public void onClick_includeOrExcludeTaxes(View view) {
        CheckBox checkBox = (CheckBox) view;
        if (checkBox.isChecked()) {
            updateTotalCostTxt();
        }
        else {
            updateTotalCostTxt(false);
        }
    }
}
