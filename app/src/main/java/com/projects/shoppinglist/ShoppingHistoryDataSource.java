package com.projects.shoppinglist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.List;

import static com.projects.shoppinglist.ShoppingHistoryTable.*;

/**
 * Created by Aziz on 10/13/2015.
 */
public class ShoppingHistoryDataSource extends DataSource {
    private String[] allColumns = {
            DBHelper.COLUMN_ID,
            COLUMN_LABEL,
            COLUMN_DATE,
            COLUMN_ITEM_ID,
            COLUMN_ITEM_QUANTITY
    };

    private static final int COLUMN_ID_INDEX = 0;
    private static final int COLUMN_LABEL_INDEX = 1;
    private static final int COLUMN_DATE_INDEX = 2;
    private static final int COLUMN_ITEM_ID_INDEX = 3;
    private static final int COLUMN_ITEM_QUANTITY_INDEX = 4;

    public ShoppingHistoryDataSource(Context context) {
        super(context);
    }

    public ShoppingHistoryEntry addShoppingListEntry(String date, String listLabel,
                                           ShoppingItem item, int itemQuantity) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_LABEL, listLabel);
        values.put(COLUMN_DATE, date);
        values.put(COLUMN_ITEM_ID, item.get_id());
        values.put(COLUMN_ITEM_QUANTITY, itemQuantity);
        long id = database.insert(TABLE_SHOPPING_HISOTRY, null, values);

        // return null if there's already a record with the same label
        if (id == -1) {
            return null;
        }
        Cursor cursor = database.query(
                TABLE_SHOPPING_HISOTRY,
                allColumns,
                DBHelper.COLUMN_ID + " = " + id,
                null, null, null, null);
        cursor.moveToFirst();
        ShoppingHistoryEntry entry = cursorToShoppingHistoryEntry(cursor, false);
        cursor.close();
        return entry;
    }

    public List<ShoppingHistoryEntry> getAllShoppingLists() {
        List<ShoppingHistoryEntry> entries = new ArrayList<ShoppingHistoryEntry>();
        Cursor cursor = database.query(
                TABLE_SHOPPING_HISOTRY,
                allColumns,
                null, null, COLUMN_LABEL, null, COLUMN_DATE + " DESC", null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            ShoppingHistoryEntry entry = cursorToShoppingHistoryEntry(cursor, false);
            entries.add(entry);
            cursor.moveToNext();
        }
        cursor.close();
        return entries;
    }

    public List<ShoppingHistoryEntry> getItemsByLabel(String label) {
        String query = "SELECT " + ENTRY_ID_WITH_PREFIX + ", " + COLUMN_LABEL_WITH_PREFIX + ", " +
                       COLUMN_DATE_WITH_PREFIX + ", " + ITEM_ID_WITH_PREFIX + ", " +
                       COLUMN_ITEM_QUANTITY_WITH_PREFIX + ", " +
                       ShoppingItemTable.ITEM_NAME_WITH_PREFIX + ", " +
                       ShoppingItemTable.ITEM_PRICE_WITH_PREFIX +
                       " FROM " + TABLE_SHOPPING_HISOTRY + " " + PREFIX + ", " +
                       ShoppingItemTable.TABLE_SHOPPING_ITEM + " " + ShoppingItemTable.PREFIX +
                       " WHERE " +
                       COLUMN_LABEL_WITH_PREFIX + " = ?" +
                       " AND " +
                       ITEM_ID_WITH_PREFIX + " = " + ShoppingItemTable.ITEM_ID_WITH_PREFIX;

        List<ShoppingHistoryEntry> entries = new ArrayList<ShoppingHistoryEntry>();

        Cursor cursor = database.rawQuery(query, new String[]{ label } );

        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            ShoppingHistoryEntry entry = cursorToShoppingHistoryEntry(cursor, true);
            entries.add(entry);
            cursor.moveToNext();
        }
        cursor.close();
        return entries;
    }

    public boolean isShoppingLabelExists(String label) {
        return getItemsByLabel(label).size() > 0;
    }

    private ShoppingHistoryEntry cursorToShoppingHistoryEntry(Cursor cursor, boolean withQuantity) {
        ShoppingItem item = ShoppingItemDataSource.getItemById(database,
                cursor.getLong(COLUMN_ITEM_ID_INDEX));
        ShoppingHistoryEntry entry = new ShoppingHistoryEntry(cursor.getLong(COLUMN_ID_INDEX),
                item, cursor.getString(COLUMN_DATE_INDEX), cursor.getString(COLUMN_LABEL_INDEX));
        if (withQuantity) {
            entry.setQuantity(cursor.getInt(COLUMN_ITEM_QUANTITY_INDEX));
        }
        return entry;
    }

}
