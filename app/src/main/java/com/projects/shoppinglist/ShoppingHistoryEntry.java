package com.projects.shoppinglist;

/**
 * Created by Aziz on 8/30/2015.
 */
public class ShoppingHistoryEntry {
    private ShoppingItem item;
    private String _date;
    private String name;
    private long _id;
    private int quantity;

    public ShoppingHistoryEntry() {
        item = new ShoppingItem();
        _date = Util.getDateTime();
        name = _date;
    }

    public ShoppingHistoryEntry(long id, ShoppingItem item, String _date, String name) {
        this.item = item;
        this._date = _date;
        this.name = name;
        _id = id;
    }

    public String getDate() {
        return _date;
    }

    public String getName() {
        return name;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public ShoppingItem getItem() {
        return item;
    }

    public void setItem(ShoppingItem item) {
        this.item = item;
    }

    @Override
    public String toString() {
        return _date + " - " + name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
