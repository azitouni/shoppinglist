package com.projects.shoppinglist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class BrowseListsActivity extends AppCompatActivity {

    private ShoppingHistoryDataSource datasource;
    private ListView shoppingLists;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, UserPreferencesActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == android.R.id.home) {
            goToMainActivity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_lists);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        shoppingLists = (ListView) findViewById(R.id.shoppingLists);
        shoppingLists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ShoppingHistoryEntry entry = (ShoppingHistoryEntry) shoppingLists
                        .getItemAtPosition(position);
                Intent intent = new Intent(BrowseListsActivity.this, ShoppingListDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("label", entry.getName());
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
        datasource = new ShoppingHistoryDataSource(this);
        List<ShoppingHistoryEntry> values = datasource.getAllShoppingLists();
        ArrayAdapter<ShoppingHistoryEntry> adapter = new ArrayAdapter<ShoppingHistoryEntry>(this,
                android.R.layout.simple_list_item_1, values);
        shoppingLists.setAdapter(adapter);
    }
}
